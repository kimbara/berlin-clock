/**
 * 
 */
package com.inkglobal.techtest.clockparts;

import org.joda.time.LocalTime;

/**
 * FirstMinutesRow is the first minutes representing the time in the Berlin
 * Clock. It is also the third row in the clock. This row contains 11 lamps,
 * each of them representing that five full minutes have elapsed within the
 * hour. The third, sixth and ninth lamps (representing full quarters) are red,
 * all other lamps are yellow.
 * 
 * @author amarinperez
 * 
 */
public class FirstMinutesRow {
	private FifteenMinuteBlock[] fifteenMinuteBlocks;
	private TenMinuteBlock tenMinuteBlock;
	
	public FirstMinutesRow(LocalTime time) {
		fifteenMinuteBlocks = new FifteenMinuteBlock[3];
		
		for(int i = 0; i < 3; i++)
			fifteenMinuteBlocks[i] = new FifteenMinuteBlock(i, time);
		
		tenMinuteBlock = new TenMinuteBlock(time); 
	}
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		for (FifteenMinuteBlock fifteenMinuteBlock : fifteenMinuteBlocks)
			sb.append(fifteenMinuteBlock.toString());
			
		sb.append(tenMinuteBlock.toString());
		
		return sb.toString();
	}	
}
