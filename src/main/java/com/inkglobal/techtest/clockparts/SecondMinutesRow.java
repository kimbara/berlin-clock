package com.inkglobal.techtest.clockparts;

import org.joda.time.LocalTime;

/**
 * SecondMinutesRow is the second row of minutes in the Berlin Clock. This is
 * also the forth and last row of lamps in the clock. This row has four lamps,
 * all of them yellow, and each of them representing one minute has elapsed.
 * 
 * @author amarinperez
 * 
 */
public class SecondMinutesRow {
	private int minutes;

	public SecondMinutesRow(LocalTime time) {
		minutes = time.getMinuteOfHour();
	}

	@Override
	public String toString() {
		switch (minutes % 5) {
			case 0:
				return "OOOO";
			case 1:
				return "YOOO";
			case 2:
				return "YYOO";
			case 3:
				return "YYYO";
			default:
				return "YYYY";
		}
	}

}
