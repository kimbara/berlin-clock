package com.inkglobal.techtest.clockparts;

import org.joda.time.LocalTime;

/**
 * FirstHoursRow is the first row of lamps indicating the hours. It consists of
 * four red lamps, each of them representing five full hours have elapsed from
 * midnight.
 * 
 * @author amarinperez
 * 
 */
public class FirstHoursRow {
	int hours;

	public FirstHoursRow(LocalTime localTime) {
		hours = localTime.hourOfDay().get();
	}

	@Override
	public String toString() {
		if (hours < 5)
			return "OOOO";
		else if (hours < 10)
			return "ROOO";
		else if (hours < 15)
			return "RROO";
		else if (hours < 20)
			return "RRRO";
		else 
			return "RRRR";
	}
}
