/**
 * 
 */
package com.inkglobal.techtest.clockparts;

import org.joda.time.LocalTime;

/**
 * SecondHoursRow is the second row of lamps indicating the hours. It consists
 * of four red lamps, each of them representing one full hour has elapsed from
 * the hours indicated in the first row of hours.
 * 
 * @author amarinperez
 * 
 */
public class SecondHoursRow {
	int hours;

	public SecondHoursRow(LocalTime time) {
		hours = time.hourOfDay().get();
	}

	@Override
	public String toString() {
		switch (hours % 5) {
			case 0:
				return "OOOO";
			case 1:
				return "ROOO";
			case 2:
				return "RROO";
			case 3:
				return "RRRO";
			default:
				return "RRRR";
		}
	}
}
