package com.inkglobal.techtest.clockparts;

import org.joda.time.LocalTime;

/**
 * Represents the seconds lamp in a Berlin Clock. The seconds lamp is the one at
 * the top, and it lights up every two seconds.
 * 
 * @author amarinperez
 * 
 */
public class SecondsLamp {
	int seconds;

	public SecondsLamp(LocalTime time) {
		seconds = time.secondOfMinute().get();
	}

	@Override
	public String toString() {
		if (seconds % 2 == 0)
			return "Y";
		else
			return "O";
	}
}
