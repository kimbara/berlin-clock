/**
 * 
 */
package com.inkglobal.techtest.clockparts;

import org.joda.time.LocalTime;

/**
 * TenMinuteBlock is a set of two lamps at the end of the first row of minutes
 * representing ten minutes. Both lamps are yellow, and each of them represent 5
 * minutes of time. Because the lamps are the last ones in the first row of
 * minutes, they only light up after the 50th minute in the hour.
 * 
 * @author amarinperez
 * 
 */
public class TenMinuteBlock {
	int minutes;

	public TenMinuteBlock(LocalTime localTime) {
		minutes = localTime.getMinuteOfHour();
	}

	@Override
	public String toString() {
		if (minutes < 50)
			return "OO";
		else if (minutes < 55)
			return "YO";
		else
			return "YY";
	}
}
