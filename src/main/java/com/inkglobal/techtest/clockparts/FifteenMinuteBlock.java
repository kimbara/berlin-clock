/**
 * 
 */
package com.inkglobal.techtest.clockparts;

import org.joda.time.LocalTime;

/**
 * FifteenMinuteBlock is a set of three consecutive lamps in the first minutes
 * row. The three consecutive lamps will be two yellow lamps followed by a red
 * lamp. Each lamp represents five minutes have elapsed. There are three blocks
 * like this in the first minutes row.
 * 
 * Since there are three of these blocks in each line they operate with an
 * offset. In order to know the offset the block needs to know whether it is
 * first, second or third.
 * 
 * @author amarinperez
 * 
 */
public class FifteenMinuteBlock {
	private int minutes;

	/**
	 * 
	 * @param position
	 *            Zero-based position for this block. Valid values are 0, 1 and
	 *            2.
	 * @param localTime
	 */
	public FifteenMinuteBlock(int position, LocalTime localTime) {
		if (position < 0 || position > 2)
			throw new IllegalArgumentException("Position needs to be 0, 1 or 2");

		minutes = localTime.getMinuteOfHour() - 15 * position;
	}

	@Override
	public String toString() {
		if (minutes < 5)
			return "OOO";
		else if (minutes < 10)
			return "YOO";
		else if (minutes < 15)
			return "YYO";
		else
			return "YYR";
	}
}
