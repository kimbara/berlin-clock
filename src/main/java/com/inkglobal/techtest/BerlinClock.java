package com.inkglobal.techtest;

import org.joda.time.LocalTime;

import com.inkglobal.techtest.clockparts.FirstHoursRow;
import com.inkglobal.techtest.clockparts.FirstMinutesRow;
import com.inkglobal.techtest.clockparts.SecondHoursRow;
import com.inkglobal.techtest.clockparts.SecondMinutesRow;
import com.inkglobal.techtest.clockparts.SecondsLamp;

public class BerlinClock {
	private SecondsLamp secondsLamp;
	private FirstHoursRow firstHoursRow;
	private SecondHoursRow secondHoursRow;
	private FirstMinutesRow firstMinutesRow;
	private SecondMinutesRow secondMinutesRow;
	
	public BerlinClock(LocalTime time) {
		secondsLamp = new SecondsLamp(time);
		firstHoursRow = new FirstHoursRow(time);
		secondHoursRow = new SecondHoursRow(time);
		firstMinutesRow = new FirstMinutesRow(time);
		secondMinutesRow = new SecondMinutesRow(time);
	}
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		
		sb.append(secondsLamp.toString()).append(" ");
		sb.append(firstHoursRow.toString()).append(" ");
		sb.append(secondHoursRow.toString()).append(" ");
		sb.append(firstMinutesRow.toString()).append(" ");
		sb.append(secondMinutesRow.toString());
		
		return sb.toString();
	}
}
