package com.inkglobal.techtest.clockparts;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.joda.time.LocalTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import com.inkglobal.techtest.clockparts.SecondHoursRow;

@RunWith(Parameterized.class)
public class SecondHoursRowFourLightsTest {
	@Parameter
	public LocalTime time = null;

	@Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] { { new LocalTime(4, 2, 1) }, { new LocalTime(9, 2, 1) },
				{ new LocalTime(14, 2, 1) }, { new LocalTime(19, 2, 1) } });
	}

	@Test
	public void test_FourHoursFromFirstRow_LightsUpAllLamps() {
		SecondHoursRow secondHoursRow = new SecondHoursRow(time);
		assertEquals("RRRR", secondHoursRow.toString());
	}
}
