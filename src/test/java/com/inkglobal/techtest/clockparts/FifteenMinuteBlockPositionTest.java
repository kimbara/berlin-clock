package com.inkglobal.techtest.clockparts;

import org.joda.time.LocalTime;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.inkglobal.techtest.clockparts.FifteenMinuteBlock;

public class FifteenMinuteBlockPositionTest {
	@Rule public ExpectedException onError = ExpectedException.none();
	
	@Test
	public void test_AcceptValidPosition() {
		new FifteenMinuteBlock(0, new LocalTime(1,2,4)); 
	}
	
	@Test
	public void test_NegativePosition_ThrowsException()
	{
		onError.expect(IllegalArgumentException.class);
		new FifteenMinuteBlock(-1, new LocalTime(1,2,3));
	}
	
	@Test
	public void test_HighPosition_ThrowsException()
	{
		onError.expect(IllegalArgumentException.class);
		new FifteenMinuteBlock(3, new LocalTime(1,2,3));
	}
}
