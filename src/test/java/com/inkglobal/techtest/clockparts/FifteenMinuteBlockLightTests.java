package com.inkglobal.techtest.clockparts;

import static org.junit.Assert.assertEquals;

import org.joda.time.LocalTime;
import org.junit.Test;

import com.inkglobal.techtest.clockparts.FifteenMinuteBlock;

public class FifteenMinuteBlockLightTests {
	@Test
	public void test_FirstFiveMinutesInTheHour_NoLights() {
		FifteenMinuteBlock block = new FifteenMinuteBlock(0, new LocalTime(1,0,4));
		assertEquals("OOO", block.toString());
	}

	@Test
	public void test_AfetFiveMinutesButBeforeTenMinutes_OneLight()
	{
		FifteenMinuteBlock block = new FifteenMinuteBlock(0, new LocalTime(1,6,4));
		assertEquals("YOO", block.toString());
	}
	
	@Test
	public void test_AfetTenMinutesButBeforeFifteenMinutes_TwoLights()
	{
		FifteenMinuteBlock block = new FifteenMinuteBlock(0, new LocalTime(1,12,4));
		assertEquals("YYO", block.toString());
	}

	@Test
	public void test_AfetFifteenMinutes_ThreeLights()
	{
		FifteenMinuteBlock block = new FifteenMinuteBlock(0, new LocalTime(1,17,4));
		assertEquals("YYR", block.toString());
	}

	@Test
	public void test_SecondBlock_NoLights_BeforeTwentyMinutes()
	{
		FifteenMinuteBlock block = new FifteenMinuteBlock(1, new LocalTime(1,19,4));
		assertEquals("OOO", block.toString());
	}

	@Test
	public void test_ThirdBlock_NoLights_BeforeThirtyFiveMinutes()
	{
		FifteenMinuteBlock block = new FifteenMinuteBlock(2, new LocalTime(1,34,4));
		assertEquals("OOO", block.toString());
	}
}
