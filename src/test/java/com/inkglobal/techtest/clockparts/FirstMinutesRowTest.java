package com.inkglobal.techtest.clockparts;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.joda.time.LocalTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import com.inkglobal.techtest.clockparts.FirstMinutesRow;

@RunWith(Parameterized.class)
public class FirstMinutesRowTest {
	@Parameter(value = 0)
	public LocalTime time;
	
	@Parameter(value = 1)
	public String expectedLights;
	
	@Parameters
	public static Collection<Object[]> data()
	{
		return Arrays.asList(new Object[][] {
				{new LocalTime(1,3,2),  "OOOOOOOOOOO"},
				{new LocalTime(1,12,2), "YYOOOOOOOOO"},
				{new LocalTime(1,25,3), "YYRYYOOOOOO"},
				{new LocalTime(1,37,2), "YYRYYRYOOOO"},
				{new LocalTime(1,48,2), "YYRYYRYYROO"},
				{new LocalTime(1,59,2), "YYRYYRYYRYY"}
		});
	}
	
	@Test
	public void test_ExpectedLights() {
		FirstMinutesRow firstMinutesRow = new FirstMinutesRow(time);
		assertEquals(expectedLights, firstMinutesRow.toString());
	}
}
