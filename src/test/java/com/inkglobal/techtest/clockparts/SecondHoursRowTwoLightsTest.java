package com.inkglobal.techtest.clockparts;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.joda.time.LocalTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import com.inkglobal.techtest.clockparts.SecondHoursRow;

@RunWith(Parameterized.class)
public class SecondHoursRowTwoLightsTest {
	@Parameter
	public LocalTime time = null;

	@Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] { { new LocalTime(2, 2, 1) }, { new LocalTime(7, 2, 1) },
				{ new LocalTime(12, 2, 1) }, { new LocalTime(17, 2, 1) }, { new LocalTime(22, 2, 1) } });
	}

	@Test
	public void test_TwoHoursFromFirstRow_LightsUpTwoLamps() {
		SecondHoursRow secondHoursRow = new SecondHoursRow(time);
		assertEquals("RROO", secondHoursRow.toString());
	}
}
