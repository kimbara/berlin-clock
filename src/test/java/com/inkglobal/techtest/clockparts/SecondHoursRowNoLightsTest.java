package com.inkglobal.techtest.clockparts;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.joda.time.LocalTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import com.inkglobal.techtest.clockparts.SecondHoursRow;

@RunWith(Parameterized.class)
public class SecondHoursRowNoLightsTest {
	@Parameter
	public LocalTime time = null;

	@Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] { { new LocalTime(0, 2, 1) }, { new LocalTime(5, 2, 1) },
				{ new LocalTime(10, 2, 1) }, { new LocalTime(15, 2, 1) }, { new LocalTime(20, 2, 1) } });
	}

	@Test
	public void test_LessThanOneHourFromFirstRow_DoesNotLightUpLamps() {
		SecondHoursRow secondHoursRow = new SecondHoursRow(time);
		assertEquals("OOOO", secondHoursRow.toString());
	}
}
