package com.inkglobal.techtest.clockparts;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.joda.time.LocalTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import com.inkglobal.techtest.clockparts.SecondMinutesRow;

@RunWith(Parameterized.class)
public class SecondMinutesRowTwoLightsTest {
	@Parameter
	public LocalTime time = null;

	@Parameters
	public static Collection<Object[]> data() {
		Object[][] times = new Object[10][];

		for (int i = 0; i < 10; i++)
			times[i] = new Object[] { new LocalTime(13, 5 * i + 2, 23) };

		return Arrays.asList(times);
	}

	@Test
	public void test_MultiplesOfFiveMinutesPlusTwo_LightUpTwoLamps() {
		SecondMinutesRow secondHoursRow = new SecondMinutesRow(time);
		assertEquals("YYOO", secondHoursRow.toString());
	}
}
