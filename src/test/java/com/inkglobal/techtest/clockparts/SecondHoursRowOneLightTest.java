package com.inkglobal.techtest.clockparts;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.joda.time.LocalTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import com.inkglobal.techtest.clockparts.SecondHoursRow;

@RunWith(Parameterized.class)
public class SecondHoursRowOneLightTest {
	@Parameter
	public LocalTime time = null;

	@Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] { { new LocalTime(1, 2, 1) }, { new LocalTime(6, 2, 1) },
				{ new LocalTime(11, 2, 1) }, { new LocalTime(16, 2, 1) }, { new LocalTime(21, 2, 1) } });
	}

	@Test
	public void test_OneHourFromFirstRow_LightsUpOneLamp() {
		SecondHoursRow secondHoursRow = new SecondHoursRow(time);
		assertEquals("ROOO", secondHoursRow.toString());
	}
}
