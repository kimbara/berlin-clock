package com.inkglobal.techtest.clockparts;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.joda.time.LocalTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import com.inkglobal.techtest.clockparts.SecondHoursRow;

@RunWith(Parameterized.class)
public class SecondHoursRowThreeLightsTest {
	@Parameter
	public LocalTime time = null;

	@Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] { { new LocalTime(3, 2, 1) }, { new LocalTime(8, 2, 1) },
				{ new LocalTime(13, 2, 1) }, { new LocalTime(18, 2, 1) }, { new LocalTime(23, 2, 1) } });
	}

	@Test
	public void test_ThreeHoursFromFirstRow_LightsUpThreeLamps() {
		SecondHoursRow secondHoursRow = new SecondHoursRow(time);
		assertEquals("RRRO", secondHoursRow.toString());
	}
}
