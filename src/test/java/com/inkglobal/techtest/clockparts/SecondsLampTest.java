package com.inkglobal.techtest.clockparts;

import static org.junit.Assert.assertEquals;

import org.joda.time.LocalTime;
import org.junit.Test;

import com.inkglobal.techtest.clockparts.SecondsLamp;

public class SecondsLampTest {

	@Test
	public void test_EvenSeconds_LightsUpTheLamp() {
		SecondsLamp lamp = new SecondsLamp(new LocalTime(4,5,0));
		assertEquals("Y", lamp.toString());
	}

	@Test
	public void test_OddSeconds_TurnsOffTheLamp()
	{
		SecondsLamp lamp = new SecondsLamp(new LocalTime(4,5,1));
		assertEquals("O", lamp.toString());
	}
}
