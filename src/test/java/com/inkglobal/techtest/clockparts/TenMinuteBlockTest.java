package com.inkglobal.techtest.clockparts;

import static org.junit.Assert.assertEquals;

import org.joda.time.LocalTime;
import org.junit.Test;

import com.inkglobal.techtest.clockparts.TenMinuteBlock;

public class TenMinuteBlockTest {

	@Test
	public void test_NoLights_BeforeFiftyMinutes() {
		TenMinuteBlock block = new TenMinuteBlock(new LocalTime(3, 49, 3));
		assertEquals("OO", block.toString());
	}
	
	@Test
	public void test_OneLight_AfterFiftyMinutesButBeforeFityFive()
	{
		TenMinuteBlock block = new TenMinuteBlock(new LocalTime(3, 53, 3));
		assertEquals("YO", block.toString());
	}

	@Test
	public void test_TwoLights_AfterFiftyFiveMinutes()
	{
		TenMinuteBlock block = new TenMinuteBlock(new LocalTime(3, 55, 3));
		assertEquals("YY", block.toString());
	}
}
