package com.inkglobal.techtest.clockparts;

import static org.junit.Assert.*;

import org.joda.time.LocalTime;
import org.junit.Test;

import com.inkglobal.techtest.clockparts.FirstHoursRow;

public class FirstHoursRowTest {

	@Test
	public void test_TimeBeforeFiveAm_DoesNotUseAnyLamps() {
		FirstHoursRow firstHoursRow = new FirstHoursRow(new LocalTime(3, 2, 3));
		assertEquals("OOOO", firstHoursRow.toString());
	}

	@Test
	public void test_TimeAfterFiveButBeforeTen_LightsUpOneLamp() {
		FirstHoursRow firstHoursRow = new FirstHoursRow(new LocalTime(7, 2, 3));
		assertEquals("ROOO", firstHoursRow.toString());
	}
	
	@Test
	public void test_TimeAfterTenButBeforeThreePm_LightsUpTwoLamps()
	{
		FirstHoursRow firstHoursRow = new FirstHoursRow(new LocalTime(12, 2, 3));
		assertEquals("RROO", firstHoursRow.toString());
	}
	
	@Test
	public void test_TimeAfterThreePmButBeforeEightPm_LightsUpThreeLamps()
	{
		FirstHoursRow firstHoursRow = new FirstHoursRow(new LocalTime(15, 2, 3));
		assertEquals("RRRO", firstHoursRow.toString());
	}
	
	@Test
	public void test_TimeAfterEightPm_LightsUpAllLamps()
	{
		FirstHoursRow firstHoursRow = new FirstHoursRow(new LocalTime(21, 2, 3));
		assertEquals("RRRR", firstHoursRow.toString());
	}
}
