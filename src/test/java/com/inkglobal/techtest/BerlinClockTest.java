package com.inkglobal.techtest;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.joda.time.LocalTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class BerlinClockTest {
	@Parameter(value = 0)
	public LocalTime time;
	
	@Parameter(value = 1)
	public String expectedLights;
	
	@Parameters
	public static Collection<Object[]> data()
	{
		return Arrays.asList(new Object[][] {
				{new LocalTime(0,0,0), "Y OOOO OOOO OOOOOOOOOOO OOOO"},
				{new LocalTime(13,17,1), "O RROO RRRO YYROOOOOOOO YYOO"},
				{new LocalTime(23,59,59), "O RRRR RRRO YYRYYRYYRYY YYYY"}
		});
	}
	
	@Test
	public void test_ExpectedLights() {
		BerlinClock berlinClock = new BerlinClock(time);
		assertEquals(expectedLights, berlinClock.toString());
	}
}
