BERLIN CLOCK

Implementation by Abraham Marin-Perez



NOTES
1. The instructions required all classes to be tested, although it didn't mandate any particular testing library. A hint was given by stating a format for the "given/when/then" clauses, which is typical of BDD libraries like jBehave. However, I interpreted this to be just an example of formatting, and jBehave isn't used as part of this project (nor it is any other BDD library).
2. The design of the Berlin Uhr leaves room for some ambiguity, since 00:00 and 24:00 are technically the same time and yet they could be expressed in two different ways:

00:00:00 Y OOOO OOOO OOOOOOOOOOO OOOO
24:00:00 Y RRRR RRRR OOOOOOOOOOO OOOO

This means that any particular time between midnight and 01:00 can be shown by this clock in two different ways:

00:13:45 O OOOO OOOO YYOOOOOOOOO YYYO
24:13:45 O RRRR RRRR YYOOOOOOOOO YYYO

It is understood that a real clock wouldn't allow such ambiguity and only one way to show the time would be used. I unsuccessfully tried to find out the way the Berlin Uhr actually shows the time between midnight and 01:00, for which I decided to choose the first format as valid.

This means that, contrary to the example given in the instructions, I have decided that 24:00:00 is just a different way to represent 00:00:00, and in order to reduce ambiguity only the latter format will be valid. 


3. There were no instructions regarding the use of third party libraries. I understand the objective of the exercise to be the conversion between times and a configuration of lamps, not the handling of time itself, for which I have considered using the library joda-time to ease the task.


